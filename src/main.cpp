#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <FS.h>
#include <Wire.h>

#define MOSI_PIN D2
#define CLK_PIN D1
#define LATCH_PIN D0

IPAddress local_IP(192,168,4,22);
IPAddress gateway(192,168,4,9);
IPAddress subnet(255,255,255,0);

void _Driver_Setup() {
    pinMode(MOSI_PIN, OUTPUT);
    pinMode(CLK_PIN, OUTPUT);
    pinMode(LATCH_PIN, OUTPUT);

    digitalWrite(LATCH_PIN, LOW);
    digitalWrite(MOSI_PIN, LOW);
    digitalWrite(LATCH_PIN, LOW);
}

void _Driver_Write12(uint16_t byte) {
    digitalWrite(MOSI_PIN, LOW);

    // 12-bits
    for (uint16_t bit = 0x0800; bit != 0; bit >>= 1) {

        if (byte & bit){
            digitalWrite(MOSI_PIN, HIGH);
        } else {
            digitalWrite(MOSI_PIN, LOW);
        }

        digitalWrite(CLK_PIN, HIGH);
        digitalWrite(CLK_PIN, LOW);
    }
}

void _Driver_LatchPulse(){    
    digitalWrite(LATCH_PIN, HIGH);
    digitalWrite(LATCH_PIN, LOW);
}

// Set LED GPIO
const int ledPin = 2;
// LED state
String ledState;
// Brightness value
int brightness = 30;


// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Replaces placeholder with LED state value
String processor(const String& var){
  Serial.println(var);
  if(var == "STATE"){
    if(digitalRead(ledPin)){
      ledState = "ON";
    }
    else{
      ledState = "OFF";
    }
    Serial.println(ledState);
    return ledState;
  }
  if (var == "BRIGHTNESS") {
    char brightnessString[3];
    return itoa(brightness, brightnessString, 10);
  }
  return "";
}
 
void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);
  _Driver_Setup();
  pinMode(ledPin, OUTPUT);

  // Initialize Wi-Fi point
  // Serial.print("\nSetting Wi-Fi configuration ...");
  // Serial.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "\nReady" : "\nFailed!");

  Serial.print("\nSetting Wi-Fi ... ");
  Serial.println(WiFi.softAP("ESP_Lights") ? "\nReady" : "\nFailed!");

  Serial.print("\nSoft-AP IP address = ");
  Serial.println(WiFi.softAPIP());

  // Initialize SPIFFS
  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  
  // Route to load style.css file
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/style.css", "text/css");
  });

  // Route to set GPIO to HIGH
  server.on("/on", HTTP_GET, [](AsyncWebServerRequest *request){
    digitalWrite(ledPin, HIGH);
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  
  // Route to set GPIO to LOW
  server.on("/off", HTTP_GET, [](AsyncWebServerRequest *request){
    digitalWrite(ledPin, LOW);
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  // Route to set Brightness
  server.on("/brightness", HTTP_GET, [](AsyncWebServerRequest *request){
    AsyncWebParameter* p = request->getParam(0);

    const int val = p->value().toInt();
 
    Serial.print("Brightness value received: ");
    Serial.println(val);

    brightness = val;

    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  // Start server
  server.begin();
}


int color = 0;
int led = 0;
int currentLine = 0;

void loop(){
  for(int i = 0; i < 48; i++) {
    // int diod = (int)i / 3;
    // int line = (int)i / 12;
    // int gradientLine = currentLine;

    if (ledState == "ON") {
      _Driver_Write12(0xFF * (brightness * 0.01));
    }
    else {
      _Driver_Write12(0x0);
    }
  }

  _Driver_LatchPulse();

  if (currentLine == 3) {
    color = color < 2 ? color++ : 0;
  }
  currentLine = currentLine < 79 ? currentLine++: 0;
  led = led < 15 ? led++ : 0;

  delay(32);
}

// Upload fs:
// platformio run --target uploadfs